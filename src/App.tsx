import React from "react";
import {fromLonLat} from "ol/proj";
import "ol/ol.css";
import {RMap, ROSM, RStyle} from "rlayers";
import {Geometry, Point} from "ol/geom";
import {Feature} from "ol";
import "./App.css"
import {uPins} from "./initial_data/initial";
import GeoJSONLayer from "./GeoJSONLayer";
import PointsLayer from "./PointsLayer";
import Detail from "./Detail";

export default function Features(): JSX.Element {
    const [currentIdx, setCurrentIdx] = React.useState<number>(0);

    const [uFeatures, setUFeatures] = React.useState<Feature[]>(
        () => {
            let unique_id = 0;
            return uPins.map(
                (pin) => {
                    return new Feature(
                        {
                            side: pin[1].side,
                            uid: unique_id++,
                            detail: pin[1].brigade,
                            geometry: new Point(fromLonLat(pin[0]))
                        })
                }
            )
        }
    );
    const [current, setCurrent] = React.useState(
        null as Feature<Geometry> | null
    )
    const [displayDetail, setDisplayDetail] = React.useState(false);
    // @ts-ignore
    return (
        <React.Fragment>
            <div className={"map"}>
                <Detail displayDetail={displayDetail} setDisplayDetail={setDisplayDetail} currentIdx={currentIdx}
                        current={current}/>
                <RMap
                    className="example-map"
                    initial={{center: fromLonLat([32.5563, 48.528]), zoom: 6}}
                    width={"100%"}
                    height={"100vh"}
                >
                    <ROSM/>
                    <GeoJSONLayer url={"https://raw.githubusercontent.com/MoscowArea/ujson/main/red.geojson"}
                                  setCurrent={setCurrent} current={current}>
                        <RStyle.RStyle>
                            <RStyle.RStroke color="#FF0000" width={3}/>
                            <RStyle.RFill color="rgba(255, 0, 0, 0.1)"/>
                        </RStyle.RStyle>
                    </GeoJSONLayer>
                    <GeoJSONLayer url={"https://raw.githubusercontent.com/MoscowArea/ujson/main/blue.geojson"}
                                  setCurrent={setCurrent} current={current}>
                        <RStyle.RStyle>
                            <RStyle.RStroke color="#0000FF" width={3}/>
                            <RStyle.RFill color="rgba(0, 0, 255, 0.1)"/>
                        </RStyle.RStyle>
                    </GeoJSONLayer>
                    <PointsLayer features={uFeatures} setCurrentIdx={setCurrentIdx}
                                 setDisplayDetail={setDisplayDetail}/>
                </RMap>
            </div>
            <button className="display-detail-btn" onClick={() => setDisplayDetail(!displayDetail)}>Подробная
                информация
            </button>
        </React.Fragment>
    );
}