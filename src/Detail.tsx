import {uPins} from "./initial_data/initial";
import React from "react";
import {Feature} from "ol";
import {Geometry} from "ol/geom";

type DetailProps = {
    displayDetail: boolean
    setDisplayDetail: CallableFunction
    currentIdx: number
    current: Feature<Geometry> | null
}

export default function Detail({displayDetail, setDisplayDetail, currentIdx, current}: DetailProps) {
    return <React.Fragment>
        <div className={"detail-panel"} style={{display: displayDetail ? "flex" : "none"}}>
            <div>
                <div className={"detail-header"}>
                    <h3>Подробная информация</h3>
                    <button className={"close-detail-btn"} onClick={() => setDisplayDetail(false)}>X</button>
                </div>
                <div className="card ms-2 me-2">
                    <div className={"card-header"}>
                        <h3>{uPins[currentIdx][1].brigade}</h3>
                    </div>
                    <div className="card-body">
                        <a href={uPins[currentIdx][1].link}><h5
                            className="card-title">Источник: {uPins[currentIdx][1].source}</h5></a>
                        <p className="card-text" style={{
                            maxHeight: "20rem",
                            overflowX: "hidden"
                        }}>{uPins[currentIdx][1].postText.replaceAll('\\n', '').replaceAll("\\", "")}</p>
                    </div>
                    <div className={"card-footer"}>
                        <h6>{uPins[currentIdx][1].date.toString()}</h6>
                    </div>
                </div>
            </div>
            <div>
                <h3>Выбранная область</h3>
                <div>
                    {current?.get("name:ru")}
                </div>
            </div>
        </div>
    </React.Fragment>
}