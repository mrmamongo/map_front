export type DetailInfo = {
    brigade: string,
    date: Date,
    source: string,
    link: string,
    side: "ВСУ" | "ВС РФ",
    postText: string
}
export type Pair<T, K> = [T, K];
