import {RFeature, RLayerVector, ROverlay, RStyle} from "rlayers";
import {Feature} from "ol";
// @ts-ignore
import ukraine_icon from "./initial_data/ukraine.png";
// @ts-ignore
import russia_icon from "./initial_data/russia.png";
import React from "react";

type PointsLayerProps = {
    features: Feature[]
    setCurrentIdx: CallableFunction
    setDisplayDetail: CallableFunction
}
export default function PointsLayer({features, setCurrentIdx, setDisplayDetail}: PointsLayerProps) {
    return <React.Fragment><RLayerVector>
        {
            features.map((feature: Feature) => (
                <RFeature
                    key={feature.get("uid")}
                    feature={feature}
                    onClick={(event) => {
                        const idx = features.findIndex(
                            (x) => x.get('uid') === event.target.get('uid')
                        );
                        setCurrentIdx(idx);
                    }}
                    onDblClick={(event) => {
                        event.preventDefault();
                        const idx = features.findIndex(
                            (x) => x.get('uid') === event.target.get('uid')
                        );
                        setCurrentIdx(idx);
                        setDisplayDetail(true);
                    }}
                >
                    <ROverlay className={"example-overlay"}>
                        <strong>{feature.get("detail")}</strong>
                    </ROverlay>
                    <RStyle.RStyle>
                        <RStyle.RIcon src={feature.get('side') == "ВСУ" ? ukraine_icon : russia_icon} scale={0.7}/>
                    </RStyle.RStyle>
                </RFeature>
            ))
        }
    </RLayerVector></React.Fragment>
}