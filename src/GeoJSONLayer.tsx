import {RLayerVector} from "rlayers";
import GeoJSON from "ol/format/GeoJSON";
import React, {useCallback} from "react";
import {Feature} from "ol";
import {Geometry} from "ol/geom";

type GeoJSONLayer = {
    url: string
    setCurrent: CallableFunction
    current: Feature<Geometry> | null
    children: React.ReactNode
}

export default function GeoJSONLayer ({url, current, setCurrent, children}: GeoJSONLayer) {
    return <React.Fragment>
    <RLayerVector
        zIndex={10}
        format={new GeoJSON({featureProjection: "EPSG:3857"})}
        url={url}
        onPointerEnter={useCallback(
            (e: { target: React.SetStateAction<Feature<Geometry> | null>; }) => {
                setCurrent(e.target)
            },
            [current]
        )}
    >
        {children}
    </RLayerVector></React.Fragment>
}