import React, {ReactElement} from 'react'
import ReactDOM from 'react-dom/client'
// @ts-ignore
import App from './App.tsx'
import "bootstrap"
import "bootstrap/dist/css/bootstrap.css"
// @ts-ignore
ReactDOM.createRoot(document.getElementById('root') as ReactElement).render(
    <App />
)
